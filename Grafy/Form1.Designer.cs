﻿namespace Grafy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.groupBoxGraf = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBoxGenerowanie = new System.Windows.Forms.GroupBox();
            this.buttonWspol = new System.Windows.Forms.Button();
            this.buttonLosowo = new System.Windows.Forms.Button();
            this.buttonKolo = new System.Windows.Forms.Button();
            this.groupBoxRecznie = new System.Windows.Forms.GroupBox();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonAddGraph = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBoxChBox = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.numericUpDownWieszchołki = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItemFile = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSave = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSavePDF = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemSaveGRF = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemExit = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemGraph = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemManual = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripMenuItemString = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItemFullSearch = new System.Windows.Forms.ToolStripMenuItem();
            this.checkedListBoxGraphs = new System.Windows.Forms.CheckedListBox();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.labelSeries = new System.Windows.Forms.Label();
            this.labelSortedSeries = new System.Windows.Forms.Label();
            this.labelConnected = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.checkBoxCheckAll = new System.Windows.Forms.CheckBox();
            this.groupBoxGraf.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBoxGenerowanie.SuspendLayout();
            this.groupBoxRecznie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWieszchołki)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // groupBoxGraf
            // 
            this.groupBoxGraf.Controls.Add(this.pictureBox1);
            this.groupBoxGraf.Location = new System.Drawing.Point(306, 27);
            this.groupBoxGraf.Name = "groupBoxGraf";
            this.groupBoxGraf.Size = new System.Drawing.Size(520, 530);
            this.groupBoxGraf.TabIndex = 1;
            this.groupBoxGraf.TabStop = false;
            this.groupBoxGraf.Text = "Graf (Wierzchołki można przesówać!!!)";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox1.Location = new System.Drawing.Point(9, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(500, 500);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox1_Paint);
            this.pictureBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseDown);
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            this.pictureBox1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseUp);
            // 
            // groupBoxGenerowanie
            // 
            this.groupBoxGenerowanie.Controls.Add(this.buttonWspol);
            this.groupBoxGenerowanie.Controls.Add(this.buttonLosowo);
            this.groupBoxGenerowanie.Controls.Add(this.buttonKolo);
            this.groupBoxGenerowanie.Location = new System.Drawing.Point(12, 203);
            this.groupBoxGenerowanie.Name = "groupBoxGenerowanie";
            this.groupBoxGenerowanie.Size = new System.Drawing.Size(288, 47);
            this.groupBoxGenerowanie.TabIndex = 3;
            this.groupBoxGenerowanie.TabStop = false;
            this.groupBoxGenerowanie.Text = "Genetowanie";
            // 
            // buttonWspol
            // 
            this.buttonWspol.Location = new System.Drawing.Point(165, 16);
            this.buttonWspol.Name = "buttonWspol";
            this.buttonWspol.Size = new System.Drawing.Size(89, 23);
            this.buttonWspol.TabIndex = 2;
            this.buttonWspol.Text = "Współrzędne?";
            this.buttonWspol.UseVisualStyleBackColor = true;
            // 
            // buttonLosowo
            // 
            this.buttonLosowo.Location = new System.Drawing.Point(84, 16);
            this.buttonLosowo.Name = "buttonLosowo";
            this.buttonLosowo.Size = new System.Drawing.Size(75, 23);
            this.buttonLosowo.TabIndex = 1;
            this.buttonLosowo.Text = "Losowo";
            this.buttonLosowo.UseVisualStyleBackColor = true;
            this.buttonLosowo.Click += new System.EventHandler(this.buttonLosowo_Click);
            // 
            // buttonKolo
            // 
            this.buttonKolo.Location = new System.Drawing.Point(3, 16);
            this.buttonKolo.Name = "buttonKolo";
            this.buttonKolo.Size = new System.Drawing.Size(75, 23);
            this.buttonKolo.TabIndex = 0;
            this.buttonKolo.Text = "Koło";
            this.buttonKolo.UseVisualStyleBackColor = true;
            this.buttonKolo.Click += new System.EventHandler(this.buttonKolo_Click);
            // 
            // groupBoxRecznie
            // 
            this.groupBoxRecznie.Controls.Add(this.buttonSave);
            this.groupBoxRecznie.Controls.Add(this.buttonAddGraph);
            this.groupBoxRecznie.Controls.Add(this.label3);
            this.groupBoxRecznie.Controls.Add(this.groupBoxChBox);
            this.groupBoxRecznie.Controls.Add(this.label1);
            this.groupBoxRecznie.Controls.Add(this.numericUpDownWieszchołki);
            this.groupBoxRecznie.Enabled = false;
            this.groupBoxRecznie.Location = new System.Drawing.Point(12, 256);
            this.groupBoxRecznie.Name = "groupBoxRecznie";
            this.groupBoxRecznie.Size = new System.Drawing.Size(288, 301);
            this.groupBoxRecznie.TabIndex = 6;
            this.groupBoxRecznie.TabStop = false;
            this.groupBoxRecznie.Text = "Dane wprowadzane ręcznie";
            // 
            // buttonSave
            // 
            this.buttonSave.Enabled = false;
            this.buttonSave.Location = new System.Drawing.Point(141, 16);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(83, 23);
            this.buttonSave.TabIndex = 6;
            this.buttonSave.Text = "Zapisz zmianę";
            this.buttonSave.UseVisualStyleBackColor = true;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonAddGraph
            // 
            this.buttonAddGraph.Location = new System.Drawing.Point(60, 16);
            this.buttonAddGraph.Name = "buttonAddGraph";
            this.buttonAddGraph.Size = new System.Drawing.Size(75, 23);
            this.buttonAddGraph.TabIndex = 5;
            this.buttonAddGraph.Text = "Dodaj graf";
            this.buttonAddGraph.UseVisualStyleBackColor = true;
            this.buttonAddGraph.Click += new System.EventHandler(this.buttonAddGraph_Click);
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(62, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 14);
            this.label3.TabIndex = 3;
            this.label3.Text = "1     2    3     4    5     6     7     8    9    10";
            // 
            // groupBoxChBox
            // 
            this.groupBoxChBox.Location = new System.Drawing.Point(52, 78);
            this.groupBoxChBox.Name = "groupBoxChBox";
            this.groupBoxChBox.Size = new System.Drawing.Size(215, 217);
            this.groupBoxChBox.TabIndex = 2;
            this.groupBoxChBox.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 42);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Liczba wierzchołków";
            // 
            // numericUpDownWieszchołki
            // 
            this.numericUpDownWieszchołki.Location = new System.Drawing.Point(6, 19);
            this.numericUpDownWieszchołki.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDownWieszchołki.Name = "numericUpDownWieszchołki";
            this.numericUpDownWieszchołki.Size = new System.Drawing.Size(48, 20);
            this.numericUpDownWieszchołki.TabIndex = 0;
            this.numericUpDownWieszchołki.ValueChanged += new System.EventHandler(this.numericUpDownWieszchołki_ValueChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemFile,
            this.toolStripMenuItemGraph});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(842, 24);
            this.menuStrip1.TabIndex = 8;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItemFile
            // 
            this.toolStripMenuItemFile.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemLoad,
            this.toolStripMenuItemSave,
            this.toolStripSeparator2,
            this.toolStripMenuItemExit});
            this.toolStripMenuItemFile.Name = "toolStripMenuItemFile";
            this.toolStripMenuItemFile.Size = new System.Drawing.Size(38, 20);
            this.toolStripMenuItemFile.Text = "Plik";
            // 
            // toolStripMenuItemLoad
            // 
            this.toolStripMenuItemLoad.Name = "toolStripMenuItemLoad";
            this.toolStripMenuItemLoad.Size = new System.Drawing.Size(171, 22);
            this.toolStripMenuItemLoad.Text = "Wczytaj graf/grafy";
            this.toolStripMenuItemLoad.Click += new System.EventHandler(this.toolStripMenuItemLoad_Click);
            // 
            // toolStripMenuItemSave
            // 
            this.toolStripMenuItemSave.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemSavePDF,
            this.toolStripMenuItemSaveGRF});
            this.toolStripMenuItemSave.Name = "toolStripMenuItemSave";
            this.toolStripMenuItemSave.Size = new System.Drawing.Size(171, 22);
            this.toolStripMenuItemSave.Text = "Zapisz graf/grafy";
            // 
            // toolStripMenuItemSavePDF
            // 
            this.toolStripMenuItemSavePDF.Name = "toolStripMenuItemSavePDF";
            this.toolStripMenuItemSavePDF.Size = new System.Drawing.Size(222, 22);
            this.toolStripMenuItemSavePDF.Text = ".pdf (aktualnie wyświetlony)";
            this.toolStripMenuItemSavePDF.Click += new System.EventHandler(this.toolStripMenuItemSavePDF_Click);
            // 
            // toolStripMenuItemSaveGRF
            // 
            this.toolStripMenuItemSaveGRF.Name = "toolStripMenuItemSaveGRF";
            this.toolStripMenuItemSaveGRF.Size = new System.Drawing.Size(222, 22);
            this.toolStripMenuItemSaveGRF.Text = ".grf";
            this.toolStripMenuItemSaveGRF.Click += new System.EventHandler(this.toolStripMenuItemSaveGRF_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(168, 6);
            // 
            // toolStripMenuItemExit
            // 
            this.toolStripMenuItemExit.Name = "toolStripMenuItemExit";
            this.toolStripMenuItemExit.Size = new System.Drawing.Size(171, 22);
            this.toolStripMenuItemExit.Text = "Wyjście";
            this.toolStripMenuItemExit.Click += new System.EventHandler(this.toolStripMenuItemExit_Click);
            // 
            // toolStripMenuItemGraph
            // 
            this.toolStripMenuItemGraph.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemManual,
            this.toolStripSeparator1,
            this.toolStripMenuItemString,
            this.toolStripMenuItemFullSearch});
            this.toolStripMenuItemGraph.Name = "toolStripMenuItemGraph";
            this.toolStripMenuItemGraph.Size = new System.Drawing.Size(41, 20);
            this.toolStripMenuItemGraph.Text = "Graf";
            // 
            // toolStripMenuItemManual
            // 
            this.toolStripMenuItemManual.Name = "toolStripMenuItemManual";
            this.toolStripMenuItemManual.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemManual.Text = "Ręcznie";
            this.toolStripMenuItemManual.Click += new System.EventHandler(this.toolStripMenuItemManual_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(149, 6);
            // 
            // toolStripMenuItemString
            // 
            this.toolStripMenuItemString.Name = "toolStripMenuItemString";
            this.toolStripMenuItemString.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemString.Text = "Z ciągu";
            this.toolStripMenuItemString.Click += new System.EventHandler(this.toolStripMenuItemString_Click);
            // 
            // toolStripMenuItemFullSearch
            // 
            this.toolStripMenuItemFullSearch.Name = "toolStripMenuItemFullSearch";
            this.toolStripMenuItemFullSearch.Size = new System.Drawing.Size(152, 22);
            this.toolStripMenuItemFullSearch.Text = "Full search";
            this.toolStripMenuItemFullSearch.Click += new System.EventHandler(this.toolStripMenuItemFullSearch_Click);
            // 
            // checkedListBoxGraphs
            // 
            this.checkedListBoxGraphs.FormattingEnabled = true;
            this.checkedListBoxGraphs.Location = new System.Drawing.Point(12, 27);
            this.checkedListBoxGraphs.Name = "checkedListBoxGraphs";
            this.checkedListBoxGraphs.Size = new System.Drawing.Size(167, 169);
            this.checkedListBoxGraphs.TabIndex = 9;
            this.checkedListBoxGraphs.SelectedValueChanged += new System.EventHandler(this.checkedListBoxGraphs_SelectedValueChanged);
            // 
            // buttonDelete
            // 
            this.buttonDelete.Location = new System.Drawing.Point(188, 46);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(109, 23);
            this.buttonDelete.TabIndex = 10;
            this.buttonDelete.Text = "Usuń zaznaczone";
            this.buttonDelete.UseVisualStyleBackColor = true;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            // 
            // labelSeries
            // 
            this.labelSeries.AutoSize = true;
            this.labelSeries.Location = new System.Drawing.Point(188, 121);
            this.labelSeries.Name = "labelSeries";
            this.labelSeries.Size = new System.Drawing.Size(13, 13);
            this.labelSeries.TabIndex = 11;
            this.labelSeries.Text = "0";
            // 
            // labelSortedSeries
            // 
            this.labelSortedSeries.AutoSize = true;
            this.labelSortedSeries.Location = new System.Drawing.Point(188, 147);
            this.labelSortedSeries.Name = "labelSortedSeries";
            this.labelSortedSeries.Size = new System.Drawing.Size(13, 13);
            this.labelSortedSeries.TabIndex = 12;
            this.labelSortedSeries.Text = "0";
            // 
            // labelConnected
            // 
            this.labelConnected.AutoSize = true;
            this.labelConnected.Location = new System.Drawing.Point(233, 170);
            this.labelConnected.Name = "labelConnected";
            this.labelConnected.Size = new System.Drawing.Size(29, 13);
            this.labelConnected.TabIndex = 13;
            this.labelConnected.Text = "false";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(185, 170);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "Spójny:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(188, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(91, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Posortowany ciąg";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(188, 108);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 13);
            this.label5.TabIndex = 16;
            this.label5.Text = "Ciąg";
            // 
            // checkBoxCheckAll
            // 
            this.checkBoxCheckAll.AutoSize = true;
            this.checkBoxCheckAll.Location = new System.Drawing.Point(188, 27);
            this.checkBoxCheckAll.Name = "checkBoxCheckAll";
            this.checkBoxCheckAll.Size = new System.Drawing.Size(115, 17);
            this.checkBoxCheckAll.TabIndex = 17;
            this.checkBoxCheckAll.Text = "Zaznacz wszystkie";
            this.checkBoxCheckAll.UseVisualStyleBackColor = true;
            this.checkBoxCheckAll.CheckedChanged += new System.EventHandler(this.checkBoxCheckAll_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(842, 564);
            this.Controls.Add(this.checkBoxCheckAll);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelConnected);
            this.Controls.Add(this.labelSortedSeries);
            this.Controls.Add(this.labelSeries);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.checkedListBoxGraphs);
            this.Controls.Add(this.groupBoxRecznie);
            this.Controls.Add(this.groupBoxGenerowanie);
            this.Controls.Add(this.groupBoxGraf);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBoxGraf.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBoxGenerowanie.ResumeLayout(false);
            this.groupBoxRecznie.ResumeLayout(false);
            this.groupBoxRecznie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownWieszchołki)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.GroupBox groupBoxGraf;
        private System.Windows.Forms.GroupBox groupBoxGenerowanie;
        private System.Windows.Forms.Button buttonWspol;
        private System.Windows.Forms.Button buttonLosowo;
        private System.Windows.Forms.Button buttonKolo;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBoxRecznie;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDownWieszchołki;
        private System.Windows.Forms.GroupBox groupBoxChBox;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFile;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemLoad;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSave;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemExit;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSavePDF;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemSaveGRF;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemGraph;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemManual;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemString;
        private System.Windows.Forms.CheckedListBox checkedListBoxGraphs;
        private System.Windows.Forms.Button buttonAddGraph;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonDelete;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemFullSearch;
        private System.Windows.Forms.Label labelSeries;
        private System.Windows.Forms.Label labelSortedSeries;
        private System.Windows.Forms.Label labelConnected;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox checkBoxCheckAll;
    }
}

