﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafy
{
    public partial class FormSeries : Form
    {
        private Form1 F1;
        List<int> listVertices;
        public FormSeries(Form1 _f1)
        {
            InitializeComponent();
            F1 = _f1;
        }

        private bool HavelHakimi(List<int> _listVertices)
        {
            string strSortedVertices = "";
            foreach (int vertice in _listVertices)
            {
                strSortedVertices += vertice.ToString() + ", ";
            }
            lbStepByStep.Items.Add(strSortedVertices);

            int numberOfVertices = _listVertices.Count;

            // Fist Step
            foreach (int vertice in _listVertices)
            {
                if (vertice >= numberOfVertices)
                {
                    lbStepByStep.Items.Add("# " + vertice.ToString() + " is bigger than or equal to number of all vertices, " + numberOfVertices.ToString());
                    return false;
                }
            }

            // Second Step
            int totalOddNumbers = 0;
            foreach (int vertice in _listVertices)
            {
                if (vertice % 2 == 1)
                {
                    totalOddNumbers++;
                }
            }
            if (totalOddNumbers % 2 == 1)
            {
                lbStepByStep.Items.Add("# Total odd number of vertices is odd number, " + totalOddNumbers.ToString());
                return false;
            }

            while (true)
            {

                // Third Step 
                foreach (int vertice in _listVertices)
                {
                    if (vertice < 0)
                    {
                        lbStepByStep.Items.Add("# There is a vertice smaller than 0");
                        return false;
                    }
                }

                // Forth Step
                bool isAllVerticesZero = true;
                foreach (int vertice in _listVertices)
                {
                    if (vertice != 0)
                    {
                        isAllVerticesZero = false;
                        break;
                    }
                }
                if (isAllVerticesZero)
                {
                    lbStepByStep.Items.Add("# All vertices are 0");
                    return true;
                }

                // Fifth Step
                _listVertices = SelectionSort(_listVertices);

                lbStepByStep.Items.Add("# After sorting");
                strSortedVertices = "";
                foreach (int vertice in _listVertices)
                {
                    strSortedVertices += vertice.ToString() + ", ";
                }
                lbStepByStep.Items.Add(strSortedVertices);

                // Sixth Step
                int k = _listVertices[0];

                // Seventh Step
                lbStepByStep.Items.Add("# Remove " + _listVertices[0].ToString() + " and subract 1 from " + k.ToString() + " times remaining of the new sequence");
                _listVertices.RemoveAt(0);

                // Eight Step
                for (int i = 0; i < k; i++)
                {
                    _listVertices[i]--;
                }

                strSortedVertices = "";
                foreach (int vertice in _listVertices)
                {
                    strSortedVertices += vertice.ToString() + ", ";
                }
                lbStepByStep.Items.Add(strSortedVertices);
            }

        }
        private List<int> SelectionSort(List<int> _listVertices)
        {
            int biggest;
            int temp;

            for (int i = 0; i < _listVertices.Count - 1; i++)
            {
                biggest = i;
                for (int j = i + 1; j < _listVertices.Count; j++)
                {
                    if (_listVertices[j] > _listVertices[biggest])
                    {
                        biggest = j;
                    }
                }
                if (biggest != i)
                {
                    temp = _listVertices[i];
                    _listVertices[i] = _listVertices[biggest];
                    _listVertices[biggest] = temp;
                }
            }
            return _listVertices;
        }
        private void button1_Click(object sender, EventArgs e)
        {
            listVertices = new List<int>();

            string[] series = textBox1.Text.Split(',');
            for (int i = 0; i < series.Length; i++)
            {
                listVertices.Add(Int32.Parse(series[i]));
            }
            

            lbStepByStep.Items.Clear();

            bool result = HavelHakimi(listVertices);

            if (result)
            {
                lbStepByStep.Items.Add(result.ToString().ToUpper() + ". This is a graph.");
                button2.Enabled = true;
            }
            else
            {
                lbStepByStep.Items.Add(result.ToString().ToUpper() + ". This is not a graph.");
                button2.Enabled = false;
            }
            listVertices.Clear();
            for (int i = 0; i < series.Length; i++)
            {
                listVertices.Add(Int32.Parse(series[i]));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int n = 0; n < numericUpDown1.Value; n++)
            {
                int[] Edges = TTT();
                bool[,] tab = new bool[listVertices.Count, listVertices.Count];

                for (int i = 0; i < Edges.Length; i += 2)
                {
                    tab[Edges[i] - 1, Edges[i + 1] - 1] = true;
                    tab[Edges[i + 1] - 1, Edges[i] - 1] = true;
                }
                F1.addGraph(new Graph(listVertices.Count, tab));
            }
            
        }
        public int RandomNumber (int n, int l)
        {
            Random rand = new Random();
            return rand.Next(n,l);
        }
        ///*
        public int[] TTT()
        {
            int[] verticesSeries = listVertices.ToArray();
            int[] verticesArray = new int[verticesSeries.Sum()]; // ilość wystąpień wierzchołka

            int index = 0;
            int vertexIndex = 1;

            int[] Edges = new int[verticesArray.Length]; // zbiór par grafu

            for (int i = 0; i < verticesSeries.Length; i++)
            {
                for (int j = 0; j < verticesSeries[i]; j++)
                {
                    verticesArray[index] = vertexIndex;
                    index++;
                }
                vertexIndex++;
            }

            int lastIndexOfverticesArray = verticesArray.Length - 1;
            int randomNumberIndex, edgesArrayIndex = 0;
            while (lastIndexOfverticesArray > 1)
            {
                randomNumberIndex = RandomNumber(0, lastIndexOfverticesArray - 1);

                if (conditionsToAcceptPair(verticesArray[randomNumberIndex], verticesArray[lastIndexOfverticesArray], Edges))
                {
                    int temp = verticesArray[lastIndexOfverticesArray - 1];
                    verticesArray[lastIndexOfverticesArray - 1] = verticesArray[randomNumberIndex];
                    verticesArray[randomNumberIndex] = temp;

                    Edges[edgesArrayIndex] = verticesArray[lastIndexOfverticesArray];
                    Edges[edgesArrayIndex + 1] = verticesArray[lastIndexOfverticesArray - 1];

                    lastIndexOfverticesArray -= 2;
                    edgesArrayIndex += 2;

                    if (lastIndexOfverticesArray < 2 && conditionsToAcceptPair(verticesArray[lastIndexOfverticesArray - 1], verticesArray[lastIndexOfverticesArray], Edges))
                    {
                        Edges[edgesArrayIndex] = verticesArray[lastIndexOfverticesArray - 1];
                        Edges[edgesArrayIndex + 1] = verticesArray[lastIndexOfverticesArray];
                    }
                    else if (lastIndexOfverticesArray < 2)
                    {
                        lastIndexOfverticesArray = verticesArray.Length - 1;
                        edgesArrayIndex = 0;
                        Edges = new int[verticesArray.Length];
                    }
                }
                else if (arrayHasWrongPairs(Edges))
                {
                    lastIndexOfverticesArray = verticesArray.Length - 1;
                    edgesArrayIndex = 0;
                    Edges = new int[verticesArray.Length];
                }

            }
            return Edges;
            //F1.addGraph(new Graph(listVertices.Count));
            //window.mainGraphs.Add(fillGraphWithNodes(Edges));
            //window.graphList.ItemsSource = window.mainGraphs;
            //window.graphList.Items.Refresh();
        }

        public bool conditionsToAcceptPair(int firstNumber, int secondNumber, int[] arrayOfPairs)
        {
            if (numbersAreEqual(firstNumber, secondNumber))
            {
                return false;
            }
            if (pairInArray(firstNumber, secondNumber, arrayOfPairs))
            {
                return false;
            }
            return true;
        }

        public bool numbersAreEqual(int first, int second)
        {
            return first == second;
        }

        public bool pairInArray(int first, int second, int[] array)
        {

            for (int i = 0; i < array.Length; i += 2)
            {
                if (numbersAreEqual(array[i], first) && numbersAreEqual(array[i + 1], second))
                {
                    return true;
                }
                if (numbersAreEqual(array[i], second) && numbersAreEqual(array[i + 1], first))
                {
                    return true;
                }
            }

            return false;
        }

        public bool arrayHasWrongPairs(int[] array)
        {
            for (int i = 0; i < array.Length; i += 2)
            {
                if (numberOfPairsInArray(array[i], array[i + 1], array) > 1)
                {
                    return true;
                }
                if (numberOfReversePairsInArray(array[i + 1], array[i], array) > 1)
                {
                    return true;
                }
            }
            return false;
        }

        public int numberOfPairsInArray(int first, int second, int[] array)
        {
            int count = 0;
            for (int i = 0; i < array.Length; i += 2)
            {
                if (numbersAreEqual(array[i], first) && numbersAreEqual(array[i + 1], second))
                {
                    count++;
                }
            }
            return count;
        }

        public int numberOfReversePairsInArray(int first, int second, int[] array)
        {
            int count = 0;
            for (int i = 0; i < array.Length; i += 2)
            {
                if (numbersAreEqual(array[i], second) && numbersAreEqual(array[i + 1], first))
                {
                    count++;
                }
            }
            return count;
        }

        //private Graph fillGraphWithNodes(int[] edges)
        //{
        //    Graph tempGraph = new Graph();
        //    tempGraph.Name = "Series Graph";
        //    tempGraph.Type = "Series";

        //    for (int i = 0; i < edges.Length; i += 2)
        //    {
        //        if (!tempGraph.Nodes.Select(x => x.Name).Contains(edges[i].ToString()))
        //        {
        //            tempGraph.Nodes.Add(new Node { Name = edges[i].ToString() });
        //        }
        //        if (!tempGraph.Nodes.Select(x => x.Name).Contains(edges[i + 1].ToString()))
        //        {
        //            tempGraph.Nodes.Add(new Node { Name = edges[i + 1].ToString() });
        //        }

        //        tempGraph.Nodes.Where(x => x.Name == edges[i].ToString()).FirstOrDefault().ConnectedNodes.Add(tempGraph.Nodes.Where(x => x.Name == edges[i + 1].ToString()).FirstOrDefault());
        //        tempGraph.Nodes.Where(x => x.Name == edges[i + 1].ToString()).FirstOrDefault().ConnectedNodes.Add(tempGraph.Nodes.Where(x => x.Name == edges[i].ToString()).FirstOrDefault());
        //    }

        //    return tempGraph;
        //}
        //*/
    }
}
