﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Grafy
{
    public partial class Form1 : Form
    {
        int handleRadius = 10; //rozmiar punktu 
        int mPointMoveInProgress = -1; //numer wybranego punktu
        int numberOfPoints = 0; // liczba wieszchołków/ punktów
        Vertex[] Vertices; //tablica punktów
        private Graphics myGraph; //coś do rysowania grafu 
        bool render = false; //zmienna do sprawdzania czy jakieś punkty są już narysowane na pictureBox
        Dictionary<int, Dictionary<int, CheckBox>> chBox; // Dynamiczna tablica 2D obiektów typu checkboź (ostatecznie nie działała jak zakładano)
        bool[,] CurentMatrix; // macierz przechowująca krawędzie / relacje między wieszchołkami 
        int globalW = 0; // zmienna globalna pomocnicza przechowująca wcześniejszą liczbę wieszchołków
        bool randomCheck = false; // zmienna do sprawdzenia czy wszystkie punkty mają być losowane czy tylko te które zostały dodane
        bool ring = true;
        bool ran = false;
        bool pdfSave = false;
        //bool Kopiuj = false;
        private List<Graph> Graphs = new List<Graph>();
        FormFullSearch FormFS;
        Bitmap bm;

        public Form1()
        {
            InitializeComponent();
        }
        //obiekty do otwierania i zapisu pliku
        OpenFileDialog ofd = new OpenFileDialog();
        SaveFileDialog sfd = new SaveFileDialog();

        public void addGraph(object o)
        {
            if (o != null && (o is Graph)  )
            {
                Graphs.Add((Graph)o);
                checkedListBoxGraphs.Items.Add("Graf: " + (Graphs.Count) + " Wieszchołków: " + Graphs[Graphs.Count - 1].numberOfVertices);
                checkedListBoxGraphs.SelectedIndex = 0;
                Change();
            }
            else if (o != null && (o is List<Graph>))
            {
                Graphs.AddRange((List<Graph>)o);
                checkedListBoxGraphs.Items.Clear();
                for (int i = 0; i < Graphs.Count; i++)
                {
                    checkedListBoxGraphs.Items.Add("Graf: " + (i+1) + " Wieszchołków: " + Graphs[i].numberOfVertices);
                }
                checkedListBoxGraphs.SelectedIndex = 0;
                Change();
            }
        }
        private void toolStripMenuItemLoad_Click(object sender, EventArgs e)
        {
            //funkcja wczytująca dane z pliku
            //string daneZPliku;
            ofd.Filter = "GRF|*.grf";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                string FileName = ofd.FileName;
                string SaveFileName = ofd.SafeFileName;
                StreamReader streamReader = new StreamReader(FileName);
                //daneZPliku = streamReader.ReadToEnd();
                string line = "";

                while ((line = streamReader.ReadLine()) != null)
                {
                    int numberOfLoadVer = 0;

                    numberOfLoadVer = line.Split(';').Length - 1;
                    bool[,] tab = new bool[numberOfLoadVer, numberOfLoadVer];
                    line = line.Replace(':', ' ');
                    line = line.Replace(';', ' ');
                    int k = 0;
                    bool error = false;
                    for (int i = 0; i < numberOfLoadVer; i++)
                    {
                        for (int j = 0; j < numberOfLoadVer; j++)
                        {
                            if (line[k] == '1')
                            {
                                tab[i, j] = true;
                                k++;
                            }
                            else if (line[k] == '0')
                            {
                                tab[i, j] = false;
                                k++;
                            }
                            else if (line[k] == ' ')
                            {
                                k++;
                                j--;
                            }
                            else
                            {
                                error = true;
                                break;
                            }
                        }

                        if (error)
                            break;
                    }

                    if (error)
                        MessageBox.Show("Plik jest nie właściwy lub uszkodzony", "Uwaga",
                                     MessageBoxButtons.OK,
                                     MessageBoxIcon.Warning);
                    else
                        addGraph(new Graph(numberOfLoadVer, tab));
                }

                //weryfikacja czy dane w pliku są poprawne 
                streamReader.Close();
            }
        }
        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            //funkcja sprawdza który wieszchołek został wybrany 
            if (render)
            {
                // Determine if a point is under the cursor. If so, declare that a move is in progress
                for (int i = 0; i < Vertices.Count(); i++)
                {
                    if (Math.Abs(e.X - Vertices[i].point.X) < handleRadius && Math.Abs(e.Y - Vertices[i].point.Y) < handleRadius)
                    {
                        mPointMoveInProgress = i;
                        break;
                    }
                    else mPointMoveInProgress = -1;
                }
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            //funkcja pozwala na przesunięcie wieszchołka gdy myszka jest nad nim oraz została wciśnieta
            if (render)
            {
                if (mPointMoveInProgress != -1) // If moving point
                {
                    if (e.X > 0 && e.X < pictureBox1.Size.Width)
                        Vertices[mPointMoveInProgress].setX(e.X);
                    if (e.Y > 0 && e.Y < pictureBox1.Size.Height)
                        Vertices[mPointMoveInProgress].setY(e.Y);

                    draw();
                }

                // If moving in the PictureBox: change cursor to hand if above a handle
                foreach (var vertex in Vertices)
                {
                    if (Math.Abs(e.X - vertex.point.X) < handleRadius && Math.Abs(e.Y - vertex.point.Y) < handleRadius)
                        Cursor.Current = Cursors.Hand;
                }
                // Cursor.Current = Cursors.Default;
            }
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            //funkcja wykrywa puszczenie wieszchołka
            // Declare that no move is in progress
            mPointMoveInProgress = -1;
        }
        private void bitmap()
        {
            //graf wstępnie rysowany jest na bitmapie
            bm = new Bitmap(pictureBox1.Size.Width, pictureBox1.Size.Height);
            //bm.SetResolution(720.0F, 720.0F);
            Graphics myBitmap = Graphics.FromImage(bm);
            myBitmap.FillRectangle(Brushes.White, 0, 0, pictureBox1.Width, pictureBox1.Height);
            myBitmap.DrawRectangle(Pens.Black, 0, 0, pictureBox1.Width - 1, pictureBox1.Height - 1);
            myBitmap.SmoothingMode = SmoothingMode.AntiAlias;
            pictureBox1.Refresh();

            for (int i = 0; i < numberOfPoints; i++)
            {
                for (int k = i; k < numberOfPoints; k++)
                {
                    if (CurentMatrix[i, k])
                        myBitmap.DrawLine(new Pen(Color.Red, 2), Vertices[i].point, Vertices[k].point); //rysowanie krawędzi na podstawie relacji
                }
            }
            //kwadraty zamiast koła
            //Rectangle rectangle;
            int j = 1;
            foreach (var vertex in Vertices)
            {
                ////rysowanie wieszchołków oraz ich numerów
                //rectangle = new Rectangle(point.X - handleRadius, point.Y - handleRadius, handleRadius * 2, handleRadius * 2);
                //myBitmap.FillRectangle(Brushes.Green, rectangle);
                myBitmap.FillEllipse(Brushes.Green, vertex.point.X - handleRadius, vertex.point.Y - handleRadius, handleRadius * 2, handleRadius * 2);
                //myBitmap.DrawRectangle(Pens.Black, rectangle);
                myBitmap.DrawEllipse(Pens.Black, vertex.point.X - handleRadius, vertex.point.Y - handleRadius, handleRadius * 2, handleRadius * 2);
                if (j<10)
                    myBitmap.DrawString(j.ToString(), new Font("Arial", 14), new SolidBrush(Color.Black), new Point(vertex.point.X - handleRadius + 2, vertex.point.Y - handleRadius));
                else
                    myBitmap.DrawString(j.ToString(), new Font("Arial", 11), new SolidBrush(Color.Black), new Point(vertex.point.X - handleRadius + 1 , vertex.point.Y - handleRadius + 1));
                j++;
            }
        }
        private void draw()
        {
            if (numberOfPoints > 0)
            {
                // funkcja odpowiada za narysowanie grafu na obiekcie pictureBox
                render = true;
                bitmap();
                pictureBox1.Image = bm; //wklejenie bitmapy do pictureBoxa
            }
        }
        public void clearPicture()
        {
            bm = new Bitmap(pictureBox1.Size.Width, pictureBox1.Size.Height);
            Graphics myBitmap = Graphics.FromImage(bm);
            myBitmap.FillRectangle(Brushes.White, 0, 0, pictureBox1.Width, pictureBox1.Height);
            myBitmap.DrawRectangle(Pens.Black, 0, 0, pictureBox1.Width - 1, pictureBox1.Height - 1);
            myBitmap.SmoothingMode = SmoothingMode.AntiAlias;
            pictureBox1.Refresh();
            pictureBox1.Image = bm;
        }
        private void buttonLosowo_Click(object sender, EventArgs e)
        {

            //z pliku trzba pobrać dane tajie jak liczba wieszchołków oraz relacje
            //numberOfPoints=
            //matryca[numberOfPoints,numberOfPoints]=
            //losowo();
            randomCheck = false; //zmiana stanu by punkty zostały wygenerowane ponownie
            randomly();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }
        private void randomly()
        {
            ring = false;
            ran = true;
            //funkcja losowo ustawia pozycje wieszchołków 
            if (randomCheck == false) //sprawdza czy to pierwsze losowanie lub nowe
            {
                Vertices = new Vertex[numberOfPoints];
                Random rand = new Random();
                for (int i = 0; i < numberOfPoints; i++)
                {
                    Vertices[i] = new Vertex(rand.Next(0, pictureBox1.Size.Width), rand.Next(0, pictureBox1.Size.Height));
                    //Points[i] = new Point(rand.Next(0, pictureBox1.Size.Width), rand.Next(0, pictureBox1.Size.Height));
                }
                randomCheck = true;
            }
            else
            {
                //losowanie pozycji tylko dla nowych wieszchołków oraz kopiowanie starych do nowej tablicy 
                Vertex[] tmp = new Vertex[Vertices.Length];
                for (int i = 0; i < Vertices.Length; i++)
                {
                    tmp[i] = new Vertex(Vertices[i].point.X, Vertices[i].point.Y);
                }
                Vertices = new Vertex[numberOfPoints];
                if (tmp.Length < Vertices.Length)
                    for (int i = 0; i < tmp.Length; i++)
                    {
                        Vertices[i] = new Vertex(tmp[i].point.X, tmp[i].point.Y);
                    }
                else
                    for (int i = 0; i < Vertices.Length; i++)
                    {
                        Vertices[i] = new Vertex(tmp[i].point.X, tmp[i].point.Y); ;
                    }
                Random rand = new Random();
                for (int i = tmp.Length; i < numberOfPoints; i++)
                {
                    Vertices[i] = new Vertex(rand.Next(0, pictureBox1.Size.Width), rand.Next(0, pictureBox1.Size.Height));
                }
            }
            myGraph = pictureBox1.CreateGraphics();
            myGraph.SmoothingMode = SmoothingMode.AntiAlias;//wygładzenie lini
            draw();
        }
        private void onRing()
        {
            ring = true;
            ran = false;
            if (numberOfPoints > 0)
            {
                // funkcja oblicza pozycje wieszchołka na podstawie ilości punktów 
                Vertices = new Vertex[numberOfPoints];
                // Points = new Point[numberOfPoints];
                double radious = 360 / numberOfPoints;
                double rad = Math.PI * radious / 180;
                int r = 150;
                int xS = pictureBox1.Size.Width / 2;
                int yS = pictureBox1.Size.Height / 2;
                double tmpRadious = radious;
                for (int i = 0; i < Vertices.Count(); i++)
                {
                    double x = r * (-Math.Cos(rad)) + xS;
                    double y = r * (-Math.Sin(rad)) + yS;
                    Vertices[i] = new Vertex((int)x, (int)y);
                    tmpRadious += radious;
                    rad = Math.PI * tmpRadious / 180; ;
                }

                myGraph = pictureBox1.CreateGraphics();
                myGraph.SmoothingMode = SmoothingMode.AntiAlias;//wygładzenie lini
                if (pdfSave)
                    bitmap();
                else
                    draw();
            }
        }
        private void buttonKolo_Click(object sender, EventArgs e)
        {
            //z pliku trzba pobrać dane tajie jak liczba wieszchołków oraz relacje
            //numberOfPoints=
            //matryca[numberOfPoints,numberOfPoints]=
            //naKole();
            onRing();
        }


        private void addCheckBoxy(int w, int spc, int xpos, int ypos, ref int x, ref int y)
        {
            //funkcja dodaje nowe CheckBoxy do groupBoxChBox w formie macierzy 
            if (w < 11)
            {
                label3.Width = 0;
                chBox = new Dictionary<int, Dictionary<int, CheckBox>>(); //Słownik 2D

                for (int i = 0; i < w; i++)
                {
                    label3.Width += 20;
                    chBox.Add(i, new Dictionary<int, CheckBox>());

                    for (int j = 0; j < w; j++)
                    {
                        chBox[i].Add(j, new CheckBox()); // nowy checkbox
                        chBox[i][j].Text = "";
                        chBox[i][j].Width = 20;
                        chBox[i][j].Checked = CurentMatrix[i, j]; //nadanie mu stanu według macierzy relacji
                        chBox[i][j].CheckedChanged += new EventHandler(CheckBox_CheckedChanged);
                        if (i == j)
                            chBox[i][j].Enabled = false;
                        //Ustawienie pozycji
                        y = i * spc + xpos;
                        x = j * spc + ypos;
                        chBox[i][j].Location = new Point(x, y);
                        //dodanie do Forma
                        groupBoxChBox.Controls.Add(chBox[i][j]);
                    }
                }
            }
        }
        private void copyMatrix(ref bool[,] M, int w, int globalW)
        {
            //funkcja pomagająca zmienić rozmiar matrycy z relacjami
            // globalW - stary rozmiar; w - nowy rozmiar
            //if (!Kopiuj)
            globalW = (int)Math.Sqrt(M.Length);
            bool[,] tmp = new bool[globalW, globalW];
            for (int i = 0; i < globalW; i++)
            {
                for (int j = 0; j < globalW; j++)
                {
                    tmp[i, j] = M[i, j];
                }
            }
            M = new bool[w, w];
            if (w > globalW)
                for (int i = 0; i < globalW; i++)
                {
                    for (int j = 0; j < globalW; j++)
                    {
                        M[i, j] = tmp[i, j];
                    }
                }
            else
                for (int i = 0; i < w; i++)
                {
                    for (int j = 0; j < w; j++)
                    {
                        M[i, j] = tmp[i, j];
                    }
                }

        }
        private void numericUpDownWieszchołki_ValueChanged(object sender, EventArgs e)
        {
            changeCheckBox();
        }
        void changeCheckBox()
        {
            // funkcja wykrywa zmiany w obiekcie numericUpDownWieszchołki i wprowadza zmiany w groupBoxChBox
            if (numberOfPoints < 11)
            {
                int xpos = 10;  // Position of the first Checkbox (x)
                int ypos = 10;  // Position of the first Checkbox (y)
                int spc = 20;   // Space between boxes (Default:20)
                int x = 1, y = 1;

                int w = Convert.ToInt32(numericUpDownWieszchołki.Value); // nowa liczba wieszchołków

                if (globalW == 0 && render == false) //pierwsze 
                {
                    numericUpDownWieszchołki.Minimum = 1;
                    label3.Width = 20;
                    globalW = w;
                    CurentMatrix = new bool[w, w]; // deklaracja rozmiaru matrycy
                    addCheckBoxy(w, spc, xpos, ypos, ref x, ref y); // rysowanie obiektów w groupBoxChBox
                }
                else if (w == 0 && render == true)
                {
                    numberOfPoints = 0;
                    globalW = 0;
                }
                else if (w > globalW || w < globalW || w == globalW) // jeśli wieszchołki zostały dodane 
                {
                    for (int i = 0; i < globalW; i++)
                    {
                        for (int j = 0; j < globalW; j++)
                        {
                            if (w < 11)
                                groupBoxChBox.Controls.Remove(chBox[i][j]); //usuwanie wszystkich CheckBox
                        }
                        if (w < 11)
                            chBox[i].Clear(); //czyszczenie tablicy
                    }
                    groupBoxChBox.Refresh(); //odswierzenie groupBoxChBox
                    chBox.Clear(); //czyszczenie tablicy
                    copyMatrix(ref CurentMatrix, w, globalW); //zmiana rozmiaru matrycy
                    globalW = w;
                    addCheckBoxy(w, spc, xpos, ypos, ref x, ref y);
                }
                //else if (w < globalW) //jeśli wieszchołek został usunięty 
                //{
                //    for (int i = 0; i < globalW; i++)
                //    {
                //        for (int j = 0; j < globalW; j++)
                //        {
                //            if (w < 11 && globalW < 11)
                //                groupBoxChBox.Controls.Remove(chBox[i][j]);
                //        }
                //        if (w < 11)
                //            chBox[i].Clear();
                //    }
                //    groupBoxChBox.Refresh();
                //    chBox.Clear();
                //    kopiujMatryce(ref matryca, w, globalW);
                //    globalW = w;
                //    dodajCheckBoxy(w, spc, xpos, ypos, ref x, ref y);
                //}
                //ponowne wygenerowanie grafu
                if (w != 0)
                {
                    numberOfPoints = w; //ustawienie liczby wieszchołków
                    if (ring)
                        onRing(); //rysowanie na kole
                    else if (ran)
                        randomly();   //rysowanie losowe             
                }
                //Kopiuj = true;
            }
        }
        private void CheckBox_CheckedChanged(object sender, EventArgs e)
        {
            //funkcja sprawdza który CheckBox zmienił stan i zmienia go na taki sam w CheckBox po przeciwnej stronie matrycy 
            for (int i = 0; i < globalW; i++)
            {
                for (int j = 0; j < globalW; j++)
                {
                    if (chBox[i][j] == (CheckBox)sender)
                    {
                        if (chBox[i][j].Checked == false)
                        {
                            chBox[j][i].Checked = false;
                            CurentMatrix[i, j] = false;
                            draw(); //ponowne wygenerowanie grafu 
                        }
                        else if (chBox[i][j].Checked == true)
                        {
                            chBox[j][i].Checked = true;
                            CurentMatrix[i, j] = true;
                            draw();
                        }
                    }
                }
            }
        }

        private void toolStripMenuItemManual_Click(object sender, EventArgs e)
        {
            if (numberOfPoints < 11)
                groupBoxRecznie.Enabled = true;
            else if (numberOfPoints > 10)
            {
                groupBoxRecznie.Enabled = false;
                MessageBox.Show("Edytować można tylko grafy które maja mniej niż 11 wieszhołków", "Uwaga",
                                     MessageBoxButtons.OK,
                                     MessageBoxIcon.Warning);
            }
            //checkedListBoxGraphs.Enabled = false;
        }

        private void toolStripMenuItemString_Click(object sender, EventArgs e)
        {
            groupBoxRecznie.Enabled = false;
            FormSeries FS = new FormSeries(this);
            FS.ShowDialog();
            //checkedListBoxGraphs.Enabled = true;
        }

        private void toolStripMenuItemExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonAddGraph_Click(object sender, EventArgs e)
        {
            if (numberOfPoints > 0)
            {
                Graphs.Add(new Graph(numberOfPoints, CurentMatrix));

                checkedListBoxGraphs.Items.Add("Graf: " + (Graphs.Count) + " Wieszchołków: " + Graphs[Graphs.Count - 1].numberOfVertices);
                checkedListBoxGraphs.SelectedIndex = Graphs.Count - 1;
                buttonSave.Enabled = true;
            }
        }
        public void Change()
        {
            try
            {
                int tmp = numberOfPoints;
                numberOfPoints = Graphs[checkedListBoxGraphs.SelectedIndex].numberOfVertices;
                CurentMatrix = (bool[,])Graphs[checkedListBoxGraphs.SelectedIndex].matrix.Clone();
                labelConnected.Text = Graphs[checkedListBoxGraphs.SelectedIndex].connected.ToString();
                labelSeries.Text = string.Join(",", Graphs[checkedListBoxGraphs.SelectedIndex].series);
                labelSortedSeries.Text = string.Join(",", Graphs[checkedListBoxGraphs.SelectedIndex].sortedSeries);
                //label6.Text = checkedListBoxGraphs.SelectedIndex.ToString();
                //label7.Text = Graphs.IndexOf(Graphs[checkedListBoxGraphs.SelectedIndex]).ToString();
                if (numberOfPoints == tmp)
                    changeCheckBox();
                if (numberOfPoints < 11)
                    numericUpDownWieszchołki.Value = numberOfPoints;
                if (numberOfPoints > 10)
                    groupBoxRecznie.Enabled = false;

                if (ring)
                    onRing();
                else if (ran)
                    randomly();

            }
            catch { }
        }
        private void checkedListBoxGraphs_SelectedValueChanged(object sender, EventArgs e)
        {
            Change();
        }



        private void buttonSave_Click(object sender, EventArgs e)
        {
            int tmp = checkedListBoxGraphs.SelectedIndex;
            Graphs[checkedListBoxGraphs.SelectedIndex].numberOfVertices = numberOfPoints;
            Graphs[checkedListBoxGraphs.SelectedIndex].matrix = (bool[,])CurentMatrix.Clone();
            Graphs[checkedListBoxGraphs.SelectedIndex].counSeries();
            //checkedListBoxGraphs.Items.Clear();
            //checkedListBoxGraphs.Items.RemoveAt(tmp);
            checkedListBoxGraphs.Items[tmp] = $"Graf: {(tmp + 1)} Wieszchołków: {Graphs[tmp].numberOfVertices}";
            //for (int i = 0; i < Graphs.Count(); i++)
            //{
            //    checkedListBoxGraphs.Items.Add("Graf: " + (i + 1) + " Wieszchołków: " + Graphs[i].numberOfVertices);
            //}
            if (ring)
                onRing();
            else if (ran)
                randomly();
            checkedListBoxGraphs.SelectedIndex = tmp;
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (numberOfPoints > 0 && checkedListBoxGraphs.SelectedIndex >= 0)
            {
                List<int> tmp = new List<int>();
                foreach (int item in checkedListBoxGraphs.CheckedIndices)
                {
                    tmp.Add(item);
                }
                for (int i = tmp.Count - 1; i >= 0; i--)
                {
                    Graphs.RemoveAt(tmp[i]);
                    checkedListBoxGraphs.Items.RemoveAt(tmp[i]);
                }
                if (checkedListBoxGraphs.Items.Count == 0)
                    clearPicture();
                checkBoxCheckAll.Checked = false;


            }

        }

        private void toolStripMenuItemSaveGRF_Click(object sender, EventArgs e)
        {
            if (checkedListBoxGraphs.CheckedItems.Count == 0)
            {
                MessageBox.Show("Nie wybrana żadnego grafu", "Uwaga",
                                     MessageBoxButtons.OK,
                                     MessageBoxIcon.Warning);
            }
            else
            {
                string saveFileGRF = "";
                foreach (var item in checkedListBoxGraphs.CheckedIndices)
                {
                    for (int i = 0; i < Graphs[(int)item].numberOfVertices; i++)
                    {
                        for (int j = 0; j < Graphs[(int)item].numberOfVertices; j++)
                        {
                            saveFileGRF += Convert.ToByte(Graphs[(int)item].matrix[i, j]) + ":";
                        }
                        saveFileGRF += ";";
                    }
                    saveFileGRF += "\r\n";
                }


                sfd.Filter = "GRF|*.grf";
                sfd.FileName = "Grafy";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter streamWriter = new StreamWriter(sfd.FileName);
                    streamWriter.Write(saveFileGRF);
                    MessageBox.Show("Zapisano", "Informacja",
                                     MessageBoxButtons.OK,
                                     MessageBoxIcon.Information);
                    streamWriter.Close();
                }
            }

        }



        void DrawImage(XGraphics gfx, string jpegSamplePath, int x, int y, int width, int height)
        {
            XImage image = XImage.FromFile(jpegSamplePath);
            gfx.DrawImage(image, x, y, width, height);
        }
        private void toolStripMenuItemSavePDF_Click(object sender, EventArgs e)
        {
            PdfDocument document = new PdfDocument();
            PdfPage page = document.AddPage();
            XGraphics gfx = XGraphics.FromPdfPage(page);

            //if (checkedListBoxGraphs.CheckedItems.Count == 0)
            //{
            //    MessageBox.Show("Nie wybrana żadnego grafu", "Uwaga",
            //                         MessageBoxButtons.OK,
            //                         MessageBoxIcon.Warning);
            //}
            //else
            //{
                int k = 0;
                pdfSave = true;

                //foreach (int item in checkedListBoxGraphs.CheckedIndices)
                //{
                //    numberOfPoints = Graphs[item].numberOfVertices;
                //    CurentMatrix = (bool[,])Graphs[item].matrix.Clone();

                //    onRing();
                    using (Bitmap bmp = bm)
                    {
                        bmp.Save($@"tmp.bmp", ImageFormat.Bmp);
                    }
                    DrawImage(gfx, $@"./tmp.bmp", 100, 100 + k, 400, 400);

                    k += 200;
                //}

            //}
            pdfSave = false;

            //DrawImage(gfx, @"./tmp.bmp", 50, 50, 200, 200);
            //DrawImage(gfx, @"./tmp.bmp", 250, 50, 200, 200);
            //DrawImage(gfx, @"./tmp.bmp", 50, 250, 200, 200);
            sfd.Filter = "PDF|*.pdf";
            sfd.FileName = "GrafPDF";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                document.Save(sfd.FileName);

                DialogResult dialogResult = MessageBox.Show("Plik został pomyślnie zapisany.\r\n Czy otworzyć plik PDF", "Informacja",
                                 MessageBoxButtons.YesNoCancel,
                                 MessageBoxIcon.Information);
                if (dialogResult == DialogResult.Yes)
                    Process.Start(sfd.FileName);

            }
        }

        private void toolStripMenuItemFullSearch_Click(object sender, EventArgs e)
        {
            FormFS = new FormFullSearch(this);
            //FormFullSearch FormFS = new FormFullSearch();
            //Form1 F1 = new Form1();
            FormFS.ShowDialog();

        }

        private void checkBoxCheckAll_CheckedChanged(object sender, EventArgs e)
        {

            for (int i = 0; i < checkedListBoxGraphs.Items.Count; i++)
            {
                checkedListBoxGraphs.SetItemChecked(i, checkBoxCheckAll.Checked);
            }



        }
    }


}
