﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy
{
    class Graph
    {
        public int numberOfVertices { get; set; }
        public bool[,] matrix { get; set; }
        public int[] series { get; set; }
        public int[] sortedSeries { get; set; }
        public bool connected { get; set; }
        public List<Vertex> vertexList { get; set; }
        public Graph(int nOV, bool[,]m )
        {
            vertexList = new List<Vertex>();
            numberOfVertices = nOV;
            matrix = (bool[,])m.Clone();
            counSeries();
        }
        //public void AddVertex(Point p, DragDeltaEventHandler dragDelta)
        //{
        //    vertexList.Add(new Vertex(
        //            vertexList.Count + 1,
        //            p,
        //            dragDelta));
        //}
        public void counSeries()
        {
            series = new int[numberOfVertices];
            for (int i = 0; i < numberOfVertices; i++)
            {
                int degree=0;
                for (int j = 0; j < numberOfVertices; j++)
                {
                    if (matrix[i, j])
                        degree++;
                }
                series[i] = degree;
            }
            if (series.Contains(0) || (series.Sum()<=numberOfVertices + 1 && numberOfVertices>3))
                connected = false;
            else
                connected = true;
            sortedSeries=series.OrderByDescending(c => c).ToArray();
        }

    }
}
