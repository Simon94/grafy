﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Grafy
{
    class Vertex
    {
        public Point point { get; set; }
        
        public Vertex (int x,int y)
        {
            point = new Point(x, y);
        }
        public void setX(int x)
        {
            point = new Point(x, point.Y);
        }
        public void setY(int y)
        {
            point = new Point(point.X, y);
        }
    }
}
