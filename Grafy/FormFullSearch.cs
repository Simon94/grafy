﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.Threading;

namespace Grafy
{
    public partial class FormFullSearch : Form
    {
        private Form1 F1;
        int NumberOfV;
        int numberOfByte;
        int numberOfAllGraphs;
        List<Graph> Graphs = new List<Graph>();
        Thread Thread1;
        //Thread Thread1_1;
        //Thread Thread1_2;
        public FormFullSearch(Form1 _f1)
        {
            InitializeComponent();
            F1 = _f1;
        }
        public void sendGraph()
        {
            //foreach (var graph in Graphs)
            //{
            //    F1.addGraph(graph);
            //}
            F1.addGraph(Graphs);
        }
        public void createGraph(object o)
        {
            
            int n = (int)o;
            string s = Convert.ToString(n, 2); //Convert to binary in a string

            int[] bits = s.PadLeft(numberOfByte, '0') // Add 0's from left
                         .Select(c => int.Parse(c.ToString())) // convert each char to int
                         .ToArray(); // Convert IEnumerable from select to Array
            bool[,] tab = new bool[NumberOfV, NumberOfV];

            int k = 0;
            for (int i = 0; i < NumberOfV; i++)
            {
                for (int j = i; j < NumberOfV; j++)
                {
                    if (j == i)
                        tab[i, j] = false;
                    else
                    {
                        if (bits[k] == 1)
                        {
                            tab[i, j] = true;
                            tab[j, i] = true;
                        }
                        else if (bits[k] == 0)
                        {
                            tab[i, j] = false;
                            tab[j, i] = false;
                        }
                        k++;
                    }
                }
            }

            Graph gTmp = new Graph(NumberOfV, tab);
            bool ok = false;
            if (checkBoxIzo.Checked)
            {
                bool tmp = false;
                foreach (var item in Graphs)
                {
                    if (item.sortedSeries.SequenceEqual(gTmp.sortedSeries))
                    {
                        //TestIsomorphism(item,gTmp);
                            tmp = true;
                    }
                    if (tmp)
                        break;
                        
                }
                if (!tmp && !checkBoxCon.Checked)
                {
                    Graphs.Add(new Graph(NumberOfV, tab));

                }
                else if (!tmp && checkBoxCon.Checked)
                    ok = true;
            }
            if (checkBoxCon.Checked)
            {
                if (gTmp.connected && (!checkBoxIzo.Checked || ok))
                    Graphs.Add(new Graph(NumberOfV, tab));

            }
            if (!checkBoxCon.Checked && !checkBoxIzo.Checked)
                Graphs.Add(new Graph(NumberOfV, tab));
            
        }
        public void start()
        {
            for (int i = 0; i < numberOfAllGraphs; i++)
            {
                progressBar1.Invoke(new Action(delegate ()
                {
                    progressBar1.Value = i;
                    double percent = (((double)progressBar1.Value / (double)progressBar1.Maximum) * 100);
                    percent = Math.Round(percent, 2);
                    progressBar1.CreateGraphics().DrawString(percent.ToString() + "%", new Font("Arial", (float)8.25, FontStyle.Regular), Brushes.Black, new PointF(progressBar1.Width / 2 - 10, progressBar1.Height / 2 - 7));

                    createGraph(i);
                }));

            }

            label1.Invoke(new Action(delegate ()
            {
                label1.Text = "done";
            }));
        }
        private void buttonSearch_Click(object sender, EventArgs e)
        {
            NumberOfV = (int)numericUpDown1.Value;
            numberOfByte = ((NumberOfV - 1) * NumberOfV) / 2;
            numberOfAllGraphs = (int)Math.Pow(2, numberOfByte);
            progressBar1.Maximum = numberOfAllGraphs - 1;

            Thread1 = new Thread(start);
            
            Thread1.Start();

            
        }

        private void buttonStop_Click(object sender, EventArgs e)
        {
            Thread1.Abort();
        }

        private void label1_TextChanged(object sender, EventArgs e)
        {
            sendGraph();
        }
        //private void TestIsomorphism(Graph primaryGraph, Graph secondaryGraph)
        //{

        //    worker = new AbortableBackgroundWorker();
        //    worker.WorkerSupportsCancellation = true;
        //    if (primaryGraph.vertexList.Count != 0 && secondaryGraph.vertexList.Count != 0)
        //    {
        //        if (primaryGraph.vertexList.Count == secondaryGraph.vertexList.Count)
        //        {
        //            if (EqualEdges(primaryGraph, secondaryGraph))
        //            {
        //                if (EqualVertexDegrees())
        //                {
        //                    worker.DoWork += delegate (object s, DoWorkEventArgs args)
        //                    {
        //                        GeneratePermutations(primaryGraph.vertexList.Count);
        //                    };

        //                    worker.RunWorkerCompleted += delegate (object s, RunWorkerCompletedEventArgs args)
        //                    {
        //                        Thread.Sleep(100);
        //                        resetButton.Content = "Reset";
        //                        resultLabel.Content = "";
        //                        successLabel.Content = "";
        //                        if (permutation.workingPermutationList.Count != 0)
        //                        {
        //                            resultLabel.Content = "Grafy sa izomorficzne";
        //                            resultLabel.Background = Brushes.Green;
        //                            ProoveIsomorphism();
        //                            listPermutationsButton.Visibility = Visibility.Visible;
        //                            secondaryCanvas.Visibility = Visibility.Hidden;
        //                        }
        //                        else
        //                        {
        //                            resultLabel.Content = "Grafy nie sa izomorficzne";
        //                            resultLabel.Background = Brushes.Red;
        //                        }
        //                        th.Join();
        //                    };
        //                    worker.RunWorkerAsync();
        //                }
        //                else
        //                {
        //                    resultLabel.Content = "Niezgodne stopnie wierzchołków";
        //                    resultLabel.Background = Brushes.Red;
        //                    resetButton.Content = "Reset";
        //                }
        //            }
        //            else
        //            {
        //                resultLabel.Content = "Niezgodna liczba krawędzi";
        //                resultLabel.Background = Brushes.Red;
        //                resetButton.Content = "Reset";
        //            }
        //        }
        //        else
        //        {
        //            resultLabel.Content = "Niezgodna liczba wierzchołków";
        //            resultLabel.Background = Brushes.Red;
        //            resetButton.Content = "Reset";
        //        }
        //    }
        //    else
        //    {
        //        resultLabel.Content = "Wprowadź oba grafy";
        //        resultLabel.Background = Brushes.Red;
        //        resetButton.Content = "Reset";
        //    }
        //    th = new Thread(new ThreadStart(() => UpdateProgressBar(worker)));
        //    th.Start();
        //}
        //public bool EqualEdges(Graph primaryGraph, Graph secondaryGraph)
        //{
        //    int primary = 0; int secondary = 0;
        //    primaryGraph.FillMatrix();
        //    for (int i = 1; i < primaryGraph.vertexList.Count; i++)
        //    {
        //        for (int j = 0; j < i; j++)
        //        {
        //            primary += primaryGraph.adjacencyMatrix[i][j];
        //        }
        //    }
        //    secondaryGraph.FillMatrix();
        //    for (int i = 1; i < secondaryGraph.vertexList.Count; i++)
        //    {
        //        for (int j = 0; j < i; j++)
        //        {
        //            secondary += secondaryGraph.adjacencyMatrix[i][j];
        //        }
        //    }

        //    if (primary == secondary)
        //        return true;
        //    else
        //        return false;
        //}
        //public bool EqualVertexDegrees()
        //{
        //    List<int> primary = new List<int>();
        //    List<int> secondary = new List<int>();
        //    int tmp;
        //    bool result = true;
        //    foreach (var vertex in primaryGraph.vertexList)
        //    {
        //        tmp = 0;
        //        foreach (var connection in vertex.Connections)
        //        {
        //            tmp++;
        //        }
        //        primary.Add(tmp);
        //    }

        //    foreach (var vertex in secondaryGraph.vertexList)
        //    {
        //        tmp = 0;
        //        foreach (var connection in vertex.Connections)
        //        {
        //            tmp++;
        //        }
        //        secondary.Add(tmp);
        //    }
        //    primary.Sort();
        //    secondary.Sort();

        //    for (int i = 0; i < primary.Count; i++)
        //    {
        //        if (primary[i] != secondary[i])
        //            result = false;
        //    }

        //    return result;
        //}
    }
}
